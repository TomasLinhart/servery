<?php

// Script executed by cron every 5 minutes

include('../includes/config.php');

$Database = new mysqli($db_config['host'], $db_config['user'], $db_config['pass'], $db_config['name']);

if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

function CheckPortStatus($Ip, $Port)
{
    $Timeout = 1;
    if ($Socket = @fsockopen($Ip, $Port, $ErrorNumber, $ErrorString, $Timeout)) {
        fclose($Socket);
        return (1);
    } else {
        return (0);
    }
}

$TextState = array('Offline', 'Online');

echo("Kontroluji stav serveru...<br>\n");
$PlayersCount = 0;
$OnlineCount = 0;
$OfflineCount = 0;
$Servers = $Database->query("SELECT `id`,`ip`,`port`,`name` FROM `servers`");
while ($Server = $Servers->fetch_array()) {
    echo($Server['name'] . ' - ');
    $MeasureStart = microtime(TRUE);
    $OnlineState = CheckPortStatus($Server['ip'], $Server['port']);
    $Latency = (microtime(TRUE) - $MeasureStart) * 1000;
    $OnlineCount += $OnlineState;
    $OfflineCount += 1 - $OnlineState;
    // Insert new online state
    $Database->query("INSERT INTO `servers_online` (`server`, `time`, `state`, `players_count`, `latency`) VALUES (" . $Server['id'] . ", NOW() , " . $OnlineState . ", " . $PlayersCount . ", " . $Latency . ")");

    // Delete old records
    $Database->query("DELETE FROM `servers_online` WHERE `server` = " . $Server['id'] . " AND time < DATE_SUB(NOW(), INTERVAL " . $Config['OnlineStatePeriodLength'] . " DAY)");

    // Calculate latency average
    $DbResult = $Database->query("SELECT COALESCE(AVG(`latency`), 0) as `latency` FROM `servers_online` WHERE `server` = " . $Server['id'] . " AND `state` = 1 AND `latency` != 0");
    $DbRow = $DbResult->fetch_array();
    $Latency = $DbRow['latency'];
    $DbResult->free();

    // Calculate cached percentage online state
    $DbResult = $Database->query("SELECT COALESCE(COUNT(*), 1) as `count`, COALESCE(SUM(`state`), 0) as `online` FROM `servers_online` WHERE `server` = " . $Server['id']);
    $DbRow = $DbResult->fetch_array();

    // Update servers info
    $Database->query("UPDATE `servers` SET `online` = " . round($DbRow['online'] / $DbRow['count'] * 100, 2) . ", `latency` = " . $Latency . " WHERE `id` = " . $Server['id']);
    $DbResult->free();
    echo($TextState[$OnlineState] . "<br>\n");
    flush();
}
$Servers->free();

echo('Total: ' . ($OnlineCount + $OfflineCount) . ', Online: ' . $OnlineCount . ', Offline: ' . $OfflineCount . "<br>\n");

?>
