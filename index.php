<?php

error_reporting(E_ERROR | E_PARSE);

//db
session_start();
require './includes/config.php';
$db = new tMySql;
$db->dbServer = $db_config['host'];
$db->dbUser = $db_config['user'];
$db->dbPassword = $db_config['pass'];
$db->dbName = $db_config['name'];
$db->connect();
$db->query(0, "SET NAMES UTF8");
$geted = $db->escape($_GET);
$geted = array_map("htmlspecialchars", $geted);
$posted = $db->escape($_POST);
$posted = array_map("htmlspecialchars", $posted);

require_once 'recaptchalib.php';

$publickey = "6LdgWfwSAAAAAOB2KCa5sSehfAi99hCUKQiyLpVu";
$privatekey = "6LdgWfwSAAAAAEdUhhis0sSdMX5oWoXbONLod0Ww";

//language
if (isset($_COOKIE['lang'])) {
    $lang = $_COOKIE['lang'];
} else {
    $lang = 'cs';
}

require './lang/' . $lang . '.php';

$tpl = new Html_template;
$tpl->global_template = './templates/index.html';
$tpl->output = $text;

$ban = false;
$db->query('ip_bans', 'SELECT `ip_address` FROM `ip_bans`');
while ($row = $db->fetch_assoc('ip_bans')) {
    if ($_SERVER['REMOTE_ADDR'] === $row['ip_address']) {
        $ban = true;
    }
}
if ($ban) {
    $tpl->template = './templates/ban.html';
} else {
    if (!array_key_exists('page', $geted)) {
        $geted['page'] = 'servers';
    }

    $page_info = $db->query_fetch_assoc('SELECT * FROM `pages` WHERE `page` LIKE  "' . $geted['page'] . '"');
    if (empty($page_info)) {
        $page_info = $db->query_fetch_assoc('SELECT * FROM `pages` WHERE `page` LIKE "servers"');
    }
    $page_info['tpl'] = preg_replace("/-/i", "_", $page_info['page']);

    $tpl->template = './templates/' . $page_info['tpl'] . '.html';
    $p = $page_info['url'];
    $tpl->output['top_online_data'] = '';
    $tpl->output['top_rating_data'] = '';
    if ($page_info['menu'] == 1) {
        $tpl->menu_template = './templates/menu.html';
        $tpl->output['bg_color'] = '#ffffff';

        // Filter top servers
        $Filter = '';
        if (array_key_exists('page', $_GET)) {
            if ($_GET['page'] == 'servers') {
                $Filter = 'WHERE place=1';
            }

            if ($_GET['page'] == 'a-servers') {
                $Filter = 'WHERE place=2';
            }

            if ($_GET['page'] == 'wss-partners') {
                $Filter = 'WHERE wss_partner=1';
            }

        }

        // Show top player count servers
        $db->query('top_online', 'SELECT `id`, LEFT(`name`, 25) as `name`, CAST(`online_players_sum`/`player_checks` AS UNSIGNED) as online FROM `servers` ' . $Filter . ' ORDER BY `online` DESC LIMIT ' . $Config['TopServersCount']);
        $tpl->output['top_online'] = '';
        while ($row = $db->strip($db->fetch_assoc('top_online'))) {
            $row = array_map("htmlspecialchars", $row);
            if ($row['online'] == 0) {
                $row['online'] = 0;
            }

            $tpl->output['top_online_data'] .= '<li>' . ulink('server/' . $row['id'], $row['name']) . ' - ' . $row['online'] . ' ' . $text['players'] . '</li>';
        }

        // Show top rating servers
        $db->query('top_rating', 'SELECT `id`, LEFT(`name`, 25) as `name`, `rating_sum`/`ratings` as rating FROM `servers` ' . $Filter . ' ORDER BY `rating` DESC LIMIT ' . $Config['TopServersCount']);
        $tpl->output['top_rating_data'] = '';
        while ($row = $db->strip($db->fetch_assoc('top_rating'))) {
            $row = array_map("htmlspecialchars", $row);
            $row['rating'] = round($row['rating'], 2);
            $tpl->output['top_rating_data'] .= '<li>' . ulink('server/' . $row['id'], $row['name']) . ' - ' . $row['rating'] . '</li>';
        }
        $tpl->output['mm6'] = menuitem('home', $menu['mm1']);
        $tpl->output['mm1'] = menuitem('index.php?page=wss-partners', $menu['mm2']);
        $tpl->output['mm2'] = menuitem('index.php?page=servers', $menu['mm3']);
        $tpl->output['mm3'] = menuitem('index.php?page=a-servers', $menu['mm4']);
        $tpl->output['mm4'] = menuitem('index.php?page=users', $menu['mm5']);
        $tpl->output['mm5'] = menuitem('add-server/', $menu['mm6']);
    } else {
        $tpl->output['bg_color'] = '#e6e6e6';
    }

    $tpl->output['title'] = 'WSS - ' . $page_info['title'];
    $tpl->output['url'] = $config['base_url'];
    $tpl->output['lang'] = $lang;
    if (isset($_SESSION['id'])) {
        $tpl->output['acc'] = ulink('action.php?do=logout', 'Logout');
    } else {
        $tpl->output['acc'] = window_open($config['base_url'] . 'index.php?page=register', 'Registrace', 400, 350, 400, 250) . '  |  ' . window_open($config['base_url'] . 'index.php?page=login', 'Login', 200, 140, 500, 300);
    }

    $tpl->output['top_online'] = $text['top_online'];
    $tpl->output['top_rating'] = $text['top_rating'];

    if (isset($_SESSION['id'])) {
        $user = $db->strip($db->query_fetch_assoc('SELECT * from `users` WHERE id =' . (int) $_SESSION['id']));
    }
    if (($page_info['admin'] == 1) and $user['admin'] != 1) {
        die('Hacking attemp');
    }

    require $p;
}

$tpl->flush();
