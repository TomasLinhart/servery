<?php
class tMySql
{
    private $connection;
    public $dbPassword,
    $dbUser,
    $dbName,
    $dbServer,
    $result,
    $err_handler = false,
    $log_query = false,
    $insert_addslashes = false;

    public function err($r = "", $q = "", $e = "")
    {
        if ($this->err_handler) {
            $err_handler = $this->err_handler;
            $err_handler($r, $q, $e);
        } else {
            echo "<code>MYSQL";
            if ($r) {
                echo "[$r]";
            }

            if ($q) {
                echo "Dotaz:$q";
            }

            if ($e) {
                echo "Chyba:$e";
            }

            echo "</code><br />";
        };
    }

    public function connect()
    {
        $this->connection = new mysqli("mysql5-1", "servery.1274", "stXTeiP3rjwXh3jBUTGu", "servery_1274");

        if ($this->connection->connect_errno) {
            die("Nelze se pripojit k databazi.");
        }
    }

    public function query($r, $q)
    {
        $sr = $this->connection->query($q);
        if ($sr) {
            if ($r) {
                $this->result[$r] = $sr;
            }

            if ($this->log_query) {
                err($r, $q);
            }

        } else {
            $this->err($r, $q, $this->connection->error);
        }
        return ($sr);
    }

    public function query_result($q, $line = 0, $r = 0)
    {
        $sr = $this->query(0, $q); // proved sql dotaz
        if ($sr) { // pokud je nejaky vysledek
            if ($sr->num_rows > 0) {
                $sr->data_seek($line);
                $row = $sr->fetch_row();
                $ret = $row[$r];
                $sr->free(); // uvolni výsledek
                return ($ret);
            } else {
                $sr->free(); // uvolni výsledek
                return (null);
            };
        } else {
            return (null);
        }

    }

    public function query_result_all($q, $r = 0)
    {
        $sr = $this->query(0, $q);
        if ($sr) {
            $ret = array();
            $c = $sr->num_rows;
            for ($i = 0; $i < $c; $i++) {
                $sr->data_seek($i);
                $row = $sr->fetch_row();
                $ret[] = $row[$r];
            }

            $sr->free();
            return ($ret);
        } else {
            return (null);
        }

    }

    public function update($tbl, $cols, $where = "")
    {
        $s_set = "";
        while (list($k, $v) = each($cols)) {
            if ($s_set != "") {
                $s_set .= ", ";
            }

            if (ereg('^' . $k . '(.*)$', $v)) {
                $s_set .= "`$k`=" . $v;
            } else {
                $s_set .= "`$k`='" . ($this->insert_addslashes ? addslashes($v) : $v) . "'";
            }
        };
        $this->query("", "UPDATE $tbl SET $s_set " . ($where != "" ? "WHERE $where" : ""));
    }

    public function insert($tbl, $cols)
    {
        $s_set = "";
        while (list($k, $v) = each($cols)) {
            if ($s_set != "") {
                $s_set .= ", ";
            }

            if (ereg('^' . $k . '(.*)$', $v)) {
                $s_set .= "`$k`=" . $v;
            } else {
                $s_set .= "`$k`='" . ($this->insert_addslashes ? addslashes($v) : $v) . "'";
            }
        };
        $this->query(0, "INSERT INTO $tbl SET $s_set ");
    }

    public function replace($tbl, $cols)
    {
        $s_set = "";
        while (list($k, $v) = each($cols)) {
            if ($s_set != "") {
                $s_set .= ", ";
            }

            $s_set .= "$k='" . ($this->insert_addslashes ? addslashes($v) : $v) . "'";
        };
        $this->query("", "REPLACE $tbl SET $s_set");
    }

    public function fetch_assoc($r)
    {
        if ($this->result[$r]) {
            return $this->result[$r]->fetch_assoc();
        } else {
            $this->err($r, "", "no result");
        }

    }

    public function query_fetch_assoc($q)
    {
        $sr = $this->query(0, $q);
        if ($sr) {
            $ret = $sr->fetch_assoc();
            $sr->free();
            return ($ret);
        } else {
            return (null);
        }

    }

    public function query_fetch_assoc_all($q, $index = 0)
    {
        $sr = $this->query(0, $q);
        while ($row = $sr->fetch_assoc()) {
            if ($index) {
                $ret[$row[$index]] = $row;
            } else {
                $ret[] = $row;
            }

        };
        return ($ret);
    }

    public function num_rows($r)
    {
        if ($this->result[$r]) {
            return $this->result[$r]->num_rows;
        } else {
            $this->err($r, "->num_rows", "no '$r' result");
        }
    }

    public function free_result($r)
    {
        if ($this->result[$r]) {
            $this->result[$r]->free();
        }

        unset($this->result[$r]);
    }

    public function close()
    {
        $this->connection->close();
    }

    public function insert_id()
    {
        return $this->connection->insert_id;
    }

    public function conf($a)
    {
        while (list($k, $v) = each($a)) {
            $this->$k = $v;
        };
    }

    public function escape($thing)
    {
        if (is_array($thing)) {
            $escaped = array();
            foreach ($thing as $key => $value) {
                $escaped[$key] = $this->escape($value);
            }
            return $escaped;
        }
        return $this->connection->real_escape_string($thing);
    }

    public function strip($thing)
    {
        if (is_array($thing)) {
            $escaped = array();
            foreach ($thing as $key => $value) {
                $escaped[$key] = $this->strip($value);
            }
            return $escaped;
        }
        return stripslashes($thing);
    }

}
