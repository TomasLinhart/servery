<?php
class Html_template
{
    public $output = array();
    public $start_separator = '{';
    public $end_separator = '}';
    public $template;
    public $global_template;
    public $menu_template;
    public $start_global = '[';
    public $end_global = ']';
    public $compression = 'ob_gzhandler';

    public function __construct()
    {
        ob_start($this->compression);
    }

    public function flush()
    {
        $handler = fopen($this->template, 'r');
        $template_text = fread($handler, filesize($this->template));
        fclose($handler);
        if ($this->global_template != '') {
            $global_handler = fopen($this->global_template, 'r');
            $global_text = fread($global_handler, filesize($this->global_template));
            fclose($global_handler);
            if ($this->menu_template != '') {
                $menu_handler = fopen($this->menu_template, 'r');
                $menu_text = fread($menu_handler, filesize($this->menu_template));
                fclose($menu_handler);
                $global_text = preg_replace('/\\' . $this->start_global . 'local' . '\\' . $this->end_global . "/i", $menu_text, $global_text);
            }
            $html_template = preg_replace('/\\' . $this->start_global . 'local' . '\\' . $this->end_global . "/i", $template_text, $global_text);

        } else {
            $html_template = $template_text;
        }
        foreach ($this->output as $key => $value) {
            $html_template = preg_replace("/" . $this->start_separator . $key . $this->end_separator . "/i", $value, $html_template);
        }
        echo $html_template;
        ob_flush();
    }
}
