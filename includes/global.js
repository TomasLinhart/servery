function request(url, result) {
    var xmlHttp;
    var where_result = result;
    if (window.ActiveXObject) {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    else if (window.XMLHttpRequest) {
        xmlHttp = new XMLHttpRequest();
    }

    xmlHttp.open("GET", url, true);
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4) {
            if (xmlHttp.status == 200) {
                where_result.innerHTML = xmlHttp.responseText;
            }
        }
    }
    xmlHttp.send(null);

}

function potvrd(u, q) {
    var url = u;
    var question = q;
    var ano = confirm(question);
    if (ano) {
        window.location.href = url;
    }
}
