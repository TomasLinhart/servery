<?php

function ulink($uri, $thing)
{
    global $config;
    return '<a href="' . $config['base_url'] . $uri . '">' . $thing . '</a>';
}

function menuitem($uri, $thing)
{
    global $config;
    return '<a class="menuitem" href="' . $config['base_url'] . $uri . '">' . $thing . '</a>';
}

function window_open($uri, $thing, $width, $height, $left, $top)
{
    return "<a href=\"#\" onclick=\" window.open('$uri', 'win', 'width=$width,height=$height,menubar=yes,resizable=yes,left=$left,top=$top'); return false;\">$thing</a>";
}

function is_email($a)
{
    return ereg("^.+@.+\..+$", $a);
}

function check_registr()
{
    global $posted, $db;
    $return['error'] = false;
    if ($posted['pass1'] != $posted['pass2']) {
        $return['error'] = true;
        $return['error_key'] = 'pass_error';

    }
    if (empty($posted['pass1'])) {
        $return['error'] = true;
        $return['error_key'] = 'no_pass';

    }

    $pocet = $db->query_result('SELECT COUNT(*) as pocet FROM `users` WHERE name="' . $posted['name'] . '"');
    if ($pocet == 1) {
        $return['error'] = true;
        $return['error_key'] = 'login_error';

    }
    if (empty($posted['name'])) {
        $return['error'] = true;
        $return['error_key'] = 'no_login';

    }
    if (!is_email($posted['email'])) {
        $return['error'] = true;
        $return['error_key'] = 'email_wrong';

    }
    $poce = $db->query_result('SELECT COUNT(*) as pocet FROM `users` WHERE email="' . $posted['email'] . '"');
    if ($poce == 1) {
        $return['error'] = true;
        $return['error_key'] = 'email_used';

    }

    return $return;
}

function check_server()
{
    global $_POST;
    $return['error'] = false;

    if (($_POST['web'] == 'http://') || ($_POST['web_reg'] == 'http://')) {
        $return['error'] = true;
        $return['error_msg'] = 'not_web';
    }
    if (empty($_POST['name'])) {
        $return['error'] = true;
        $return['error_msg'] = 'not_name';
    }
    $name = strtolower($_POST['name']);
    $web = strtolower($_POST['web']);
    if (strpos($name, 'psych') !== false && strpos($name, 'fun') !== false || strpos($web, "psycho-project.eu") !== false) {
        $return['error'] = true;
        $return['error_msg'] = 'not_allowed';
    }
    if (intval($_POST['sum']) > 4999 || intval($_POST['limit']) > 5000) {
        $return['error'] = true;
        $return['error_msg'] = 'too_many_players';
    }
    return $return;
}

function check_add_server()
{
    global $_POST, $posted, $db;
    $return = check_server();


    print_r($posted);

    if (empty($_POST['accept'])) {
        $return['error'] = true;
        $return['error_msg'] = 'not_accept';
    }

    $exist = $db->query_result("SELECT count(*) as pocet FROM `servers` where `name` like '%" . $posted['name'] . "%'");
    if ($exist >= 1) {
        $return['error'] = true;
        $return['error_msg'] = 'name_used';
    }
    return $return;
}

function print_nick($a)
{
    global $db, $user;
    $b = $db->strip($db->query_fetch_assoc('SELECT `name`, `admin`, `ban` FROM `users` WHERE `id`=' . (int)$a));
    if ($b['admin'] == 1) {
        $return = ulink('user/' . $a, $b['name'] . '(Admin)');
    } elseif (($b['ban'] == 1) && ($user['admin'] == 1)) {
        $return = ulink('user/' . $a, $b['name'] . '(Banned)');
    } else {
        $return = ulink('user/' . $a, $b['name']);
    }
    return $return;
}

function datum($cas)
{
    global $dny, $mesice;
    $vcera = strtotime("-1 day");
    if (date("zY") == date("zY", $cas)) {
        $dnes = "Dnes " . Date("H:i:s", $cas);
    } elseif (date("zY", $vcera) == date("zY", $cas)) {
        $dnes = "Včera " . date("H:i:s", $cas);
    } else {
        $dnes = $dny[Date("D", $cas)] . Date(" d. ", $cas) .
            $mesice[(int)Date("m", $cas)] . Date(" Y - H:i:s", $cas);
    }
    return ($dnes);
}

function check_edit_server($server_id)
{
    global $_POST, $posted, $db;
    $return = check_server();
    $exist = $db->query_result("SELECT count(*) as pocet FROM `servers` where (`name` like TRIM('" . $posted['name'] . "') AND NOT `id`=$server_id)");

    if ($exist == 1) {
        $return['error'] = true;
        $return['error_msg'] = 'name_used';
    }

    return $return;
}

function delserver($server)
{
    global $db;
    $db->query(0, 'DELETE FROM `servers` WHERE `id`=' . $server . ' LIMIT 1');
    $db->query(0, 'delete from `comments` where `server`=' . $server);
    $db->query(0, 'delete from `ratings` where `server_id`=' . $server);
}
