<?php
$text = array(
    'top_rating' => '<h3>Nejoblíbenější servery</h3>',
    'top_online' => '<h3>Největší servery</h3>',
    'banned' => 'Váš účet byl zabanován',
    'wrong_pass' => 'Špatné heslo nebo login',
    'loging_ok' => 'Úspěšně přihlášen<br />Toto okno se za okamžik zavře',
    'name' => 'Jméno',
    'pass' => 'Heslo',
    'pass_again' => 'Heslo znovu',
    'pass_ok' => 'Zadaná hesla se shodují',
    'pass_error' => 'Zadaná hesla se NEshodují',
    'login_ok' => 'Zadané jméno je v pořádku',
    'login_error' => 'Zadané jméno je již používáno',
    'no_pass' => 'Nezadali jste heslo',
    'email_ok' => 'Zadaný e-mail je v pořádku',
    'email_wrong' => 'Zadaný e-mail není ve správném formátu',
    'email_used' => 'Zadaný e-mail je již používán',
    'no_login' => 'Nezadali jste jméno',
    'subject' => 'Registrace na WoW Server Status',
    'email_body' => 'Navštivte tuto adresu<br />',
    'regiter_success' => 'Na Váš e-mail byl zaslán aktivační kód pro dokončení registrace',
    'regist_finishing_error' => 'Účet nenelezen',
    'registr_finished' => ' aktivováno <br /> <a href="' . $config['base_url'] . '">Pokračujte přihlášením</a>',
    'add_server_name_used' => 'Server s tímto názvem je již registrován',
    'add_server_not_accept' => 'Musíte souhlasit s pravidly',
    'add_server_not_name' => 'Musíte zadat jméno serveru',
    'add_server_not_allowed' => 'Server s tímto názvem není povolen',
    'captcha_error' => 'Zadaný kód není správný, akci prosím opakujte',
    'add_server_success_msg' => 'Server úspěšně přidán',
    'add_server_too_many_players' => 'Není možné, aby server měl tolik hráčů.',
    'players' => 'Hráčů',
    'info_owner' => 'Založil',
    'info_avarage_players' => 'Průměrně hráčů',
    'info_place' => 'Umístění',
    'vote_only' => 'Hlasovat mohou pouze přihlášení uživatelé a pouze jednou pro jeden server',
    'comment_h3' => 'Komentáře',
    'no_comments' => 'Zatím nebyly vloženy žádné komentáře',
    'added_comment' => 'Komentář vložen',
    'add_comment' => 'Přidej komentář',
    'deny_adding_comments' => 'Pouze přihlášení uživatelé mohou vkládat komentáře',
    'add_comment' => 'Přidej komentář',
    'server_added_info' => 'Přidáno',
    'order_by' => 'Třídit podle',
    'abroad_servers' => 'Zahraniční servery',
    'th_players' => 'Hráči',
    'th_recommend' => 'Doporučujeme',
    'th_online_state' => 'Online',
    'th_latency' => 'Odezva',
    'th_xp' => 'XP násobek',
    'th_type' => 'Typ',
    'th_version' => 'Verze',
    'th_rating' => 'Známka',
    'th_created' => 'Přidáno',
    'info_servers' => 'Servery',
    'info_num_of_servers' => 'Počet serverů',
    'info_num_of_comments' => 'Počet komentářů',
    'users' => 'Uživatelé',
    'only_logged' => 'Pouze přihlášení uživatelé mohou přidávat servery.\nPřihlašte se prosím',
    'add_server_not_web' => 'Musíte vyplnit Webovou stranku a Registrační stránku',
    'banned_msg' => "Vaše IP adresa byla zabanována, pravděpodobně z důvodu zneužívání systému.",
    'footer' => "Vytvořeno členy komunity.",
);

$text['add_server_h2'] = 'Přidej server';
$text['reg_name_server'] = 'Jméno serveru';
$text['reg_type_server'] = 'Typ serveru';
$text['reg_place_server'] = 'Umístění serveru';
$text['reg_web_page'] = 'Webové stránky serveru';
$text['reg_web_reg'] = 'Stránka pro registraci';
$text['reg_desc'] = 'Krátký popis serveru';
$text['reg_czech'] = 'V Česku';
$text['reg_abroad'] = 'V zahraničí';
$text['add_server_h3'] = 'Pravidla pro přidávání serverů';
$text['add_server_rules'] = '1. V kolonce "Jméno serveru" je pouze jeho název <br /> 2. Server zde postovaný má registrace otevřené a zadarmo<br /> 3. Server <b>NEJEDE</b> přes Hamachi síť<br />4. Všechny uvedené informace jsou pravdivé';
$text['reg_accept_rules'] = 'S těmito pravidly souhlasím';


$dny["Mon"] = "Pondělí";
$dny["Tue"] = "Úterý";
$dny["Wed"] = "Středa";
$dny["Thu"] = "Čtvrtek";
$dny["Fri"] = "Pátek";
$dny["Sat"] = "Sobota";
$dny["Sun"] = "Neděle";

$mesice = array(1 => "ledna", "února", "března",
    "dubna", "května", "června",
    "července", "srpna", "září",
    "října", "listopadu", "prosince");

$menu['mm1'] = 'Partnerský program';
$menu['mm2'] = 'Doporučujeme';
$menu['mm3'] = 'České servery';
$menu['mm4'] = 'Zahraniční servery';
$menu['mm5'] = 'Uživatelé';
$menu['mm6'] = 'Přidat server';

$text['home'] = '<h1>WoW Server Status partnerský program</h1>
<b>Co je partnerský program?</b><br />
Jednoduše řečeno vzájemná reklama mezi službou <i>servery.wowresource.eu</i> a servery samotnými. Vám napomáhá zviditelnění vašeho serveru a nám zase k získávání nových uživatelů<br />
<br />
<b>Co je k partnerskému programu potřeba?</b><br />
Umístění našeho banneru na viditelné místo na Vaší webové prezentaci.<br />
<br />
<b>Jak se mohu zapojit do partnerského programu?</b><br />
Stačí žádost zaslat e-mailem, nebo ji připíchnout  na našem fóru (<i>wowresource.eu</i>) v <a href="http://www.wowresource.eu/index.php?showtopic=26063">patřičném threadu</a>.<br />
<br />
<b>A co musím splnit pro zařazení do partnerského programu?</b><br />
&nbsp; &nbsp; 1. Zaregistrovat server na <i>servery.wowresource.eu</i><br />
&nbsp; &nbsp; 2. Vyplnit 100% všechny informace o serveru který jste zaregistrovali.<br />
&nbsp; &nbsp; 3. Umístit jeden z našich bannerů na dobře viditelném místě na webu serveru, který chcete zviditelnit.<br />
&nbsp; &nbsp; 4. Server již musí být otevřený pro hráče (ne ve stádiu vývoje).<br />
&nbsp; &nbsp; 5. Server nesmí běžet pomocí Hamachi.<br />
<br />
<b>Přihlásil jsem se a splnil podmínky, jak bude kontrola probíhat?</b><br />
Do několika dní od přihlášky prověříme splnění všech podmínek pro WSS partnerský program.<br />
<br />
<b>Náš server úspěšně prošel všemi kontrolami, co nám nabídnete?</b><br />
Nabídneme Vám tzv. „hvězdu WoWResource“, což je jednak symbolické vyjádření pravdivosti všech údajů uvedených na WSS, a také jistá známka kvality. Hvězda nebude udělována všem serverům hlásícím se o WSS partnerský program –  sítem projdou pouze ty nejlepší.<br />
<br />
<b>Bude se náš server nějakým způsobem lišit od ostatních serverů uvedených na WSS?</b><br />
Jak již bylo výše zmíněno, ve sloupci „doporučujeme“ bude symbolická ikonka červené hvězdy. Dále bude pro servery z WSS partnerského programu vyčleněna zvláštní stránka, kde budou uvedeny pouze servery s „hvězdou“. <br />
<br />
<h2>Bannery</h2>
<div class="table center">
<img src="templates/img/bannery/banner_maly.gif" border="0" />
<textarea class="banner" cols="60" rows="4"><a href="http://servery.wowresource.eu/"><img src="http://servery.wowresource.eu/templates/img/bannery/banner_maly.gif" alt="WoW Server status" border="0" /></a></textarea>
</div>
';
?>
