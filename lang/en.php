<?php
$text = array(
    'top_rating' => '<h3>Top rated servers.</h3>',
    'top_online' => '<h3>Biggest servers.</h3>',
    'banned' => 'Your account is banned.',
    'wrong_pass' => 'Wrong password or login.',
    'loging_ok' => 'Login successful<br />This window will close in few seconds.',
    'name' => 'Name',
    'pass' => 'Password',
    'pass_again' => 'Password again',
    'pass_ok' => 'Password is correct.',
    'pass_error' => 'Passwords have to MATCH',
    'login_ok' => 'This name is correct.',
    'login_error' => 'This name is already taken.',
    'no_pass' => 'You have to enter passweord.',
    'email_ok' => 'Email entered is all right.',
    'email_wrong' => 'Email entered is in incorrect form.',
    'email_used' => 'Email you entered is already registred.',
    'no_login' => 'You have to enter name.',
    'subject' => 'Registration on WoW Server Status',
    'email_body' => 'Visit this webpage.<br />',
    'regiter_success' => 'Check your e-mail for activation code.',
    'regist_finishing_error' => 'Account not found!',
    'registr_finished' => ' activated <br /> <a href="' . $config['base_url'] . '">Continue by loging in.</a>',
    'add_server_name_used' => 'Theres already server with that name.',
    'add_server_not_accept' => 'You have to agree with rules.',
    'add_server_not_name' => 'You have to fill in Server name.',
    'add_server_not_allowed' => 'Server with this name is not allowed.',
    'captcha_error' => 'Entered code is not valid, please try it again.',
    'add_server_success_msg' => 'Server successfuly added.',
    'add_server_too_many_players' => 'It is not possible to have so many players.',
    'players' => 'Players',
    'info_owner' => 'Owner',
    'info_avarage_players' => 'Avearge online players',
    'info_place' => 'Location',
    'vote_only' => 'Only registred users can vote, and only one time per server.',
    'comment_h3' => 'Comments',
    'no_comments' => 'There are no comments so far.',
    'added_comment' => 'Comment added',
    'add_comment' => 'Add comment',
    'deny_adding_comments' => 'Only registred users are able to post comments.',
    'server_added_info' => 'Added',
    'order_by' => 'Sort by',
    'abroad_servers' => 'Foreign servers',
    'th_players' => 'Players',
    'th_recommend' => 'Recommended',
    'th_online_state' => 'Online',
    'th_latency' => 'Latency',
    'th_xp' => 'XP rate',
    'th_type' => 'Type',
    'th_version' => 'Version',
    'th_rating' => 'Rating',
    'th_created' => 'Added',
    'info_servers' => 'Servers',
    'info_num_of_servers' => 'Number of servers',
    'info_num_of_comments' => 'Number of comments',
    'users' => 'Users',
    'only_logged' => 'Only registred users are able to add servers.\nLog-in, please',
    'add_server_not_web' => 'You have to fill Server webpages and Registration page in.',
    'latency' => 'Latency',
    'banned_msg' => "Your IP address was banned, probably, because you have abused the system.",
    'footer' => "Made by community.",
);

$text['add_server_h2'] = 'Add server';
$text['reg_name_server'] = 'Server name';
$text['reg_type_server'] = 'Server type';
$text['reg_place_server'] = 'Server location';
$text['reg_web_page'] = 'Server webpages';
$text['reg_web_reg'] = 'Registration page';
$text['reg_desc'] = 'Short description of server';
$text['reg_czech'] = 'Czech';
$text['reg_abroad'] = 'Foreign';
$text['add_server_h3'] = 'Rules for adding servers';
$text['add_server_rules'] = '1. As "Server name" use server name ONLY <br /> 2. Your server is free, that means people dont have to pay for accounts/being able to play<br /> 3. Server is <b>NOT</b> using Hamachi network<br />4. All these informations are valid.';
$text['reg_accept_rules'] = 'I agree with these rules.';

$dny["Mon"] = "Monday";
$dny["Tue"] = "Tuesday";
$dny["Wed"] = "Wednesday";
$dny["Thu"] = "Thursday";
$dny["Fri"] = "Friday";
$dny["Sat"] = "Saturday";
$dny["Sun"] = "Sunday";

$mesice = array(1 => "January", "February", "March",
    "April", "May", "June",
    "July", "August", "September",
    "October", "November", "December");

$menu['mm1'] = 'Partner Program';
$menu['mm2'] = 'Recommended servers';
$menu['mm3'] = 'Czech/Slovak servers';
$menu['mm4'] = 'Foreign servers';
$menu['mm5'] = 'Users';
$menu['mm6'] = 'Add server';

$text['home'] = '<h1>WoW Server Status Partner Program</h1>

<b>What is "WSS Partner Program"?</b><br />
Basicaly, its exchange of adverteisments between <i>servery.wowresource.eu</i> service and World of Warcraft servers itself. <br />
<br />
<b>What you have to do to be able to join WSS Partner Program?</b><br />
You have to put our banner on a VISIBLE place on your websites.<br />
<br />
<b>How can i join?</b><br />
By sending e-mail with your request, or by asking on our forums (<i>wowresource.eu</i>) in <a href="http://www.wowresource.eu/index.php?showtopic=26063">proper thread</a>.<br />
<br />
<b>What are the requierements of WSS Partner Program?</b><br />
&nbsp; &nbsp; 1. You have to have a server registred at <i>servery.wowresource.eu</i><br />
&nbsp; &nbsp; 2. You have to have our banner on a visible place on your websites<br />
&nbsp; &nbsp; 3. You have to provide 100% valid informations on WSS <br />
<br />
<b>I have applied, how would you check my server?</b><br />
In few days after making application, we will make sure that your server meet our requierements.<br />
<br />
<b>Our server sucesfuly passed all tests. What can you offer?</b><br />
We are offering so called „Star of WoWresource“, which indicates that all informations about your server at WSS are valid. Star of WoWresource is a mark of quality as well. We wont give it to all servers...only to the best ones.<br />
<br />
<b>Is our server going to be a bit different from other servers listed at WSS?</b><br />
As we stated above, in column "recommended" will be "Star of wowresource", which indicates you have passed our tests. <br />
<br />
<h2>Banners</h2>
<div class="table center">
<img src="templates/img/bannery/banner_maly.gif" border="0" />
<textarea class="banner" cols="60" rows="4"><a href="http://servery.wowresource.eu/"><img src="http://servery.wowresource.eu/templates/img/bannery/banner_maly.gif" alt="WoW Server status" border="0" /></a></textarea>
</div>
';
?>
