<?php
if (empty($_SESSION['id'])) {
    echo '<meta http-equiv="content-type" content="text/html; charset=utf-8"><script type="text/javascript"> alert("' . $text['only_logged'] . '"); location.href="' . $_SERVER["HTTP_REFERER"] . '"; </script>';
    die($text['only_loged']);
}
$tpl->output['add_server_h2'] = $text['add_server_h2'];
$tpl->output['form_name_server'] = $text['reg_name_server'];
$tpl->output['form_type_server'] = $text['reg_type_server'];
$tpl->output['form_place_server'] = $text['reg_place_server'];
$tpl->output['form_web_page'] = $text['reg_web_page'];
$tpl->output['form_web_reg'] = $text['reg_web_reg'];
$tpl->output['form_desc'] = $text['reg_desc'];
$tpl->output['input_czech_server'] = $text['reg_czech'];
$tpl->output['input_abroad_server'] = $text['reg_abroad'];
$tpl->output['add_server_rules_h3'] = $text['add_server_rules_h3'];
$tpl->output['add_server_rules'] = $text['add_server_rules'];
$tpl->output['accept_rules'] = $text['reg_accept_rules'];
$tpl->output['info_avarage_players'] = $text['info_avarage_players'];
$tpl->output['input_sum'] = $_POST['sum'];

$tpl->output['input_name'] = htmlspecialchars($_POST['name']);
$tpl->output['input_emu'] = $_POST['emu'];
$tpl->output['input_version'] = $_POST['version'];

$tpl->output['captcha'] = recaptcha_get_html($publickey);

switch ($_POST['type']) {
    case 'PvP':
        $tpl->output['select_pvp'] = 'SELECTED';
        $tpl->output['select_rp'] = '';
        $tpl->output['select_pvprp'] = '';
        $tpl->output['select_rppvp'] = '';
        break;
    case 'RP':
        $tpl->output['select_pvp'] = '';
        $tpl->output['select_rp'] = 'SELECTED';
        $tpl->output['select_pvprp'] = '';
        $tpl->output['select_rppvp'] = '';
        break;
    case 'RPPvP':
        $tpl->output['select_pvp'] = '';
        $tpl->output['select_rp'] = '';
        $tpl->output['select_pvprp'] = '';
        $tpl->output['select_rppvp'] = 'SELECTED';
        break;
    case 'PvPRP':
        $tpl->output['select_pvp'] = '';
        $tpl->output['select_rp'] = '';
        $tpl->output['select_pvprp'] = 'SELECTED';
        $tpl->output['select_rppvp'] = '';
        break;
    default:
        $tpl->output['select_pvp'] = 'SELECTED';
        $tpl->output['select_rp'] = '';
        $tpl->output['select_pvprp'] = '';
        $tpl->output['select_rppvp'] = '';
        break;

}
$tpl->output['input_limit'] = $_POST['limit'];
if (empty($_POST['web'])) {
    $tpl->output['input_web'] = 'http://';
} else {
    $tpl->output['input_web'] = $_POST['web'];
}

if (empty($_POST['web_reg'])) {
    $tpl->output['input_reg'] = 'http://';
} else {
    $tpl->output['input_reg'] = $_POST['web_reg'];
}

$tpl->output['input_desc'] = htmlspecialchars($_POST['desc']);
if (empty($_POST['ip'])) {
    $tpl->output['ip'] = '';
} else {
    $tpl->output['ip'] = $_POST['ip'];
}

$tpl->output['port'] = $_POST['port'];

switch ($_POST['xp']) {
    case 1:
        $tpl->output['xp1'] = 'SELECTED';
        $tpl->output['xp2'] = '';
        $tpl->output['xp3'] = '';
        $tpl->output['xp4'] = '';
        $tpl->output['xp5'] = '';
        break;
    case 2:
        $tpl->output['xp1'] = '';
        $tpl->output['xp2'] = 'SELECTED';
        $tpl->output['xp3'] = '';
        $tpl->output['xp4'] = '';
        $tpl->output['xp5'] = '';
        break;
    case 3:
        $tpl->output['xp1'] = '';
        $tpl->output['xp2'] = '';
        $tpl->output['xp3'] = 'SELECTED';
        $tpl->output['xp4'] = '';
        $tpl->output['xp5'] = '';
        break;
    case 4:
        $tpl->output['xp1'] = '';
        $tpl->output['xp2'] = '';
        $tpl->output['xp3'] = '';
        $tpl->output['xp4'] = 'SELECTED';
        $tpl->output['xp5'] = '';
        break;
    case 5:
        $tpl->output['xp1'] = '';
        $tpl->output['xp2'] = '';
        $tpl->output['xp3'] = '';
        $tpl->output['xp4'] = '';
        $tpl->output['xp5'] = 'SELECTED';
        break;
    default:
        $tpl->output['xp1'] = '';
        $tpl->output['xp2'] = '';
        $tpl->output['xp3'] = 'SELECTED';
        $tpl->output['xp4'] = '';
        $tpl->output['xp5'] = '';
        break;
}

if ($_POST['place'] == 2) {
    $tpl->output['place2'] = 'SELECTED';
    $tpl->output['place1'] = '';
} else {
    $tpl->output['place2'] = '';
    $tpl->output['place1'] = 'SELECTED';
}

if (empty($_POST)) {
    $tpl->output['error_blok'] = 'none';
    $tpl->output['form_blok'] = 'block';
    $tpl->output['success_blok'] = 'none';
} else {
    $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
    if (!$resp->is_valid) {
        $tpl->output['error_blok'] = 'block';
        $tpl->output['form_blok'] = 'block';
        $tpl->output['success_blok'] = 'none';
        $tpl->output['error_msg'] = $text['captcha_error'];
    } else {
        global $posted;

        $posted['name'] = trim($posted['name']);
        $posted['name'] = preg_replace('/!\s+!/i', ' ', $posted['name']);

        $check = check_add_server();
        if ($check['error']) {
            $tpl->output['error_blok'] = 'block';
            $tpl->output['form_blok'] = 'block';
            $tpl->output['success_blok'] = 'none';
            $tpl->output['error_msg'] = $text['add_server_' . $check['error_msg']];
        } else {
            echo "'" . $posted['name'] . "'";
            $db->insert('servers', array('name' => $posted['name'], 'online_players_sum' => $posted['sum'], 'xp' => $posted['xp'], 'created' => time('now'), 'limit' => $posted['limit'], 'port' => $posted['port'], 'ip' => $posted['ip'], 'web' => $posted['web'], 'web_reg' => $posted['web_reg'], 'owner' => $_SESSION['id'], 'type' => $posted['type'], 'desc' => $posted['desc'], 'place' => $posted['place'], 'emu' => $posted['emu'], 'version' => $posted['version']));
            $tpl->output['error_blok'] = 'none';
            $tpl->output['form_blok'] = 'none';
            $tpl->output['success_blok'] = 'block';
            $tpl->output['success_msg'] = $text['add_server_success_msg'];
        }
    }
}
