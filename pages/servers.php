<?php
$tpl->output['th_players'] = $text['th_players'];
$tpl->output['th_name'] = $text['name'];
$tpl->output['th_latency'] = $text['th_latency'];
$tpl->output['th_version'] = $text['th_version'];
$tpl->output['th_xp'] = $text['th_xp'];
$tpl->output['th_online_state'] = $text['th_online_state'];
$tpl->output['th_type'] = $text['th_type'];
$tpl->output['th_rating'] = $text['th_rating'];
$tpl->output['th_recommend'] = $text['th_recommend'];
$tpl->output['th_created'] = $text['th_created'];
$tpl->output['order_by'] = $text['order_by'];
$tpl->output['place'] = $text['info_place'];

if (!array_key_exists('order', $_GET)) {
    $_GET['order'] = 0;
}

$order_index = (int) $_GET['order'];
switch ($order_index) {
    case 0:
        $order = ' order by `name`';
        break;
    case 1:
        $order = ' order by `rating` desc ';
        break;
    case 2:
        $order = ' order by `version` desc ';
        break;
    case 3:
        $order = ' order by `type` desc ';
        break;
    case 4:
        $order = ' order by `xp` desc ';
        break;
    case 5:
        $order = ' order by `players` desc ';
        break;
    case 6:
        $order = ' order by `online` desc ';
        break;
    case 7:
        $order = ' order by ceil(`online` / 100) desc, `latency`';
        break;
    case 8:
        $order = ' order by `created` desc';
        break;
}
$tpl->output['s0'] = '';
$tpl->output['s1'] = '';
$tpl->output['s2'] = '';
$tpl->output['s3'] = '';
$tpl->output['s4'] = '';
$tpl->output['s5'] = '';
$tpl->output['s6'] = '';
$tpl->output['s7'] = '';
$tpl->output['s8'] = '';
if (empty($order_index)) {
    $tpl->output['s0'] = 'SELECTED';
} else {
    $tpl->output['s' . $order_index] = 'SELECTED';
}

$pa = new CleverPager($db, 'select `id`,`wss_partner`, LEFT(`name`, 25) as `name`, `place`, `rating_sum`/`ratings` as `rating`, `version`, `type`, `xp`, `online`, `online_players_sum`/`player_checks` as `players`,`latency` from `servers` where `place`=1 ' . $order, 'p');
$pa->PageSize = $Config['DisplayServerCountPerPage'];
$pa->PageLinksDisplayed = 5;
$pa->CountCommand = 'select count(*) as Count from `servers` where `place` =1';
$pa->DataBind();
$tpl->output['servers_data'] = '';
while ($row = $pa->GetOne()) {
    $row = array_map("htmlspecialchars", $row);

    switch ($row['xp']) {
        case 1:
            $xp = 'Very low';
            break;
        case 2:
            $xp = 'Low';
            break;
        case 3:
            $xp = 'Normal';
            break;
        case 4:
            $xp = 'High';
            break;
        case 5:
            $xp = 'Very high';
            break;
    }

    if ($row['place'] == 1) {
        $place = $text['reg_czech'];
    } else {
        $place = $text['reg_abroad'];
    }

    $tpl->output['servers_data'] .= '<tr>';
    $tpl->output['servers_data'] .= ($row['wss_partner'] == 1) ? '<td><img src="templates/img/hvezda.gif" alt="Doporučujeme" title="Doporučujeme"></td>' : '<td></td>';
    $tpl->output['servers_data'] .= '<td align="left">' . ulink('server/' . $row['id'], $row['name']) . '</td>';
    $tpl->output['servers_data'] .= '<td>' . round($row['rating'], 2) . '</td>';
    $tpl->output['servers_data'] .= '<td>' . $row['version'] . '</td>';
    $tpl->output['servers_data'] .= '<td>' . $row['type'] . '</td>';
    $tpl->output['servers_data'] .= '<td>' . $xp . '</td>';
    $tpl->output['servers_data'] .= '<td>' . (int) $row['players'] . '</td>';
    $tpl->output['servers_data'] .= '<td>' . $row['online'] . '%</td>';
    if ($row['latency'] == 0) {
        $Latency = '?';
    } else {
        $Latency = round($row['latency']) . 'ms';
    }

    $tpl->output['servers_data'] .= '<td>' . $Latency . '</td>';
    $tpl->output['servers_data'] .= '</tr>';
}
ob_start();
$pa->DrawPager();
$tpl->output['pager'] = ob_get_contents();
ob_clean();
