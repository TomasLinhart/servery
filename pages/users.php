<?php
$tpl->output['th_name'] = $text['name'];
$tpl->output['th_num_of_comments'] = $text['info_num_of_comments'];
$tpl->output['th_num_of_servers'] = $text['info_num_of_servers'];
$tpl->output['users'] = $text['users'];
/*$tpl->output['order_by'] = $text['order_by'];

$order_index = (int) $_GET['order'];
switch ($order_index)
{
case 0: $order = ' order by `name` asc '; break;
case 1: $order = ' order by `s` desc '; break;
case 2: $order = ' order by `c` desc '; break;
}
$tpl->output['s0'] = '';
$tpl->output['s1'] = '';
$tpl->output['s2'] = '';
if (empty($order_index))
{
$tpl->output['s0'] = 'SELECTED';
}
else
{
$tpl->output['s'.$order_index] = 'SELECTED';
}
 */
$sql = 'select `name`, `id` from `users`';
$pa = new CleverPager($db, $sql, 'p');
$pa->PageSize = 20;
$pa->CountCommand = 'select count(*) as Count from `users`';
$pa->PageLinksDisplayed = 5;
$pa->DataBind();
$tpl->output['data'] = '';
while ($row = $pa->GetOne()) {
    $row = array_map("htmlspecialchars", $row);

    $tpl->output['data'] .= '<tr><td align="left">' . print_nick($row['id']) . '</td><td align="center">' . $db->query_result('select count(*) as pocet from servers where owner=' . $row['id']) . '</td><td align="center">' . $db->query_result('select count(*) as pocet from comments where author=' . $row['id']) . '</td></tr>';
}
ob_start();
$pa->DrawPager();
$tpl->output['pager'] = ob_get_contents();
ob_clean();
