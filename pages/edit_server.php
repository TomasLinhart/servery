<?php
$id = (int)$_GET['str1'];
$server_info = $db->query_fetch_assoc('SELECT * FROM `servers` where `id`=' . $id);
$server_info = array_map("htmlspecialchars", $server_info);
if (($user['admin'] == 1) OR ($_SESSION['id'] === $server_info['owner'])) {
    if (isset($_POST['name'])) {
        $check = check_edit_server($id);
        if ($check['error']) {
            $tpl->output['error_blok'] = 'block';
            $tpl->output['success_blok'] = 'none';
            $tpl->output['error_msg'] = $text['add_server_' . $check['error_msg']];
        } else {
            $db->update('servers', array('name' => $posted['name'], 'online_players_sum' => $posted['sum'], 'xp' => $posted['xp'], 'limit' => $posted['limit'], 'port' => $posted['port'], 'ip' => $posted['ip'], 'web' => $posted['web'], 'web_reg' => $posted['web_reg'], 'type' => $posted['type'], 'desc' => $posted['desc'], 'place' => $posted['place'], 'emu' => $posted['emu'], 'version' => $posted['version']), '`id`=' . $id);
            $tpl->output['error_blok'] = 'none';
            $tpl->output['success_blok'] = 'block';
            $tpl->output['success_msg'] = 'Edited';
            $server_info = $db->query_fetch_assoc('SELECT * FROM `servers` where `id`=' . $id);
        }

    } else {
        $tpl->output['success_blok'] = 'none';
        $tpl->output['error_blok'] = 'none';
    }

    $tpl->output['info_avarage_players'] = $text['info_avarage_players'];
    $tpl->output['input_sum'] = $server_info['online_players_sum'];


    $tpl->output['add_server_h2'] = $text['add_server_h2'];
    $tpl->output['form_name_server'] = $text['reg_name_server'];
    $tpl->output['form_type_server'] = $text['reg_type_server'];
    $tpl->output['form_place_server'] = $text['reg_place_server'];
    $tpl->output['form_web_page'] = $text['reg_web_page'];
    $tpl->output['form_web_reg'] = $text['reg_web_reg'];
    $tpl->output['form_desc'] = $text['reg_desc'];
    $tpl->output['input_czech_server'] = $text['reg_czech'];
    $tpl->output['input_abroad_server'] = $text['reg_abroad'];
    $tpl->output['add_server_rules_h3'] = $text['add_server_rules_h3'];
    $tpl->output['add_server_rules'] = $text['add_server_rules'];
    $tpl->output['accept_rules'] = $text['reg_accept_rules'];


    $tpl->output['server_name'] = $server_info['name'];
    $tpl->output['input_name'] = $server_info['name'];
    $tpl->output['input_emu'] = $server_info['emu'];
    $tpl->output['input_version'] = $server_info['version'];

    switch ($server_info['type']) {
        case 'PvP':
            $tpl->output['select_pvp'] = 'SELECTED';
            $tpl->output['select_rp'] = '';
            $tpl->output['select_pvprp'] = '';
            $tpl->output['select_rppvp'] = '';
            break;
        case 'RP':
            $tpl->output['select_pvp'] = '';
            $tpl->output['select_rp'] = 'SELECTED';
            $tpl->output['select_pvprp'] = '';
            $tpl->output['select_rppvp'] = '';
            break;
        case 'RPPvP':
            $tpl->output['select_pvp'] = '';
            $tpl->output['select_rp'] = '';
            $tpl->output['select_pvprp'] = '';
            $tpl->output['select_rppvp'] = 'SELECTED';
            break;
        case 'PvPRP':
            $tpl->output['select_pvp'] = '';
            $tpl->output['select_rp'] = '';
            $tpl->output['select_pvprp'] = 'SELECTED';
            $tpl->output['select_rppvp'] = '';
            break;
        default:
            $tpl->output['select_pvp'] = 'SELECTED';
            $tpl->output['select_rp'] = '';
            $tpl->output['select_pvprp'] = '';
            $tpl->output['select_rppvp'] = '';
            break;


    }
    $tpl->output['input_limit'] = $server_info['limit'];
    if (empty($server_info['web'])) $tpl->output['input_web'] = 'http://'; else $tpl->output['input_web'] = $server_info['web'];
    if (empty($server_info['web_reg'])) $tpl->output['input_reg'] = 'http://'; else $tpl->output['input_reg'] = $server_info['web_reg'];
    $tpl->output['input_desc'] = $server_info['desc'];
    if (empty($server_info['ip'])) $tpl->output['ip'] = 'http://'; else $tpl->output['ip'] = $server_info['ip'];
    $tpl->output['port'] = $server_info['port'];


    switch ($server_info['xp']) {
        case 1:
            $tpl->output['xp1'] = 'SELECTED';
            $tpl->output['xp2'] = '';
            $tpl->output['xp3'] = '';
            $tpl->output['xp4'] = '';
            $tpl->output['xp5'] = '';
            break;
        case 2:
            $tpl->output['xp1'] = '';
            $tpl->output['xp2'] = 'SELECTED';
            $tpl->output['xp3'] = '';
            $tpl->output['xp4'] = '';
            $tpl->output['xp5'] = '';
            break;
        case 3:
            $tpl->output['xp1'] = '';
            $tpl->output['xp2'] = '';
            $tpl->output['xp3'] = 'SELECTED';
            $tpl->output['xp4'] = '';
            $tpl->output['xp5'] = '';
            break;
        case 4:
            $tpl->output['xp1'] = '';
            $tpl->output['xp2'] = '';
            $tpl->output['xp3'] = '';
            $tpl->output['xp4'] = 'SELECTED';
            $tpl->output['xp5'] = '';
            break;
        case 5:
            $tpl->output['xp1'] = '';
            $tpl->output['xp2'] = '';
            $tpl->output['xp3'] = '';
            $tpl->output['xp4'] = '';
            $tpl->output['xp5'] = 'SELECTED';
            break;
        default:
            $tpl->output['xp1'] = '';
            $tpl->output['xp2'] = '';
            $tpl->output['xp3'] = 'SELECTED';
            $tpl->output['xp4'] = '';
            $tpl->output['xp5'] = '';
            break;
    }

    if ($server_info['place'] == 2) {
        $tpl->output['place2'] = 'SELECTED';
        $tpl->output['place1'] = '';
    } else {
        $tpl->output['place2'] = '';
        $tpl->output['place1'] = 'SELECTED';
    }

} else {
    die('hacking atempt');
}
?>
