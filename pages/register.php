<?php

$tpl->output['form_email'] = $posted['email'];
$tpl->output['form_name'] = $posted['name'];
$tpl->output['name'] = $text['name'];
$tpl->output['pass'] = $text['pass'];
$tpl->output['pass_again'] = $text['pass_again'];

if (isset($posted['name'])) {
    $register = check_registr();
    if ($register['error']) {
        $tpl->output['blok_1'] = 'block';
        $tpl->output['blok_2'] = 'none';
        $tpl->output['blok_error'] = 'block';
        $tpl->output['error_msg'] = $text[$register['error_key']];
    } else {
        $tpl->output['blok_1'] = 'none';
        $tpl->output['blok_2'] = 'block';
        $tpl->output['blok_error'] = 'none';
        $db->insert('users', array('name' => $posted['name'], 'password' => sha1($posted['pass1']), 'email' => $posted['email']));
        $headers = "MIME-Version: 1.0  \r\n";
        $headers .= "From: registration@wowresource.eu \r\n";
        $headers .= 'Content-type: text/html; charset=utf-8 \r\n';
        $subject = $text['subject'];
        $id = $db->insert_id();
        $body = $text['email_body'] . '<br />' . $config['base_url'] . 'action.php?do=confirm&amp;id=' . ($id * $config['nasobitel']);
        mail($posted['email'], $subject, $body, $headers);
        $tpl->output['regiter_success'] = $text['regiter_success'];
    }


} else {
    $tpl->output['blok_1'] = 'block';
    $tpl->output['blok_2'] = 'none';
    $tpl->output['blok_error'] = 'none';
}
?>
