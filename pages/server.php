<?php
$tpl->output['th_players'] = $text['th_players'];
$tpl->output['th_name'] = $text['name'];
$tpl->output['th_latency'] = $text['th_latency'];
$tpl->output['th_version'] = $text['th_version'];
$tpl->output['th_xp'] = $text['th_xp'];
$tpl->output['th_online_state'] = $text['th_online_state'];
$tpl->output['th_type'] = $text['th_type'];
$tpl->output['th_rating'] = $text['th_rating'];
$tpl->output['th_recommend'] = $text['th_recommend'];
$tpl->output['server_reg_desc'] = $text['reg_desc'];
$tpl->output['info_place'] = $text['info_place'];
$tpl->output['info_avarage_players'] = $text['info_avarage_players'];
$tpl->output['info_owner'] = $text['info_owner'];

$id = (int)$_GET['str1'];
$server_info = $db->strip($db->query_fetch_assoc('SELECT *, CAST(`online_players_sum`/`player_checks` AS UNSIGNED) as `online_players` FROM `servers` WHERE `id` = ' . $id));

$server_info = array_map("htmlspecialchars", $server_info);

if ($server_info['wss_partner'] == 1) {
    $tpl->output['server_name'] = '<img src="templates/img/hvezda.gif" alt="WSS Partner" title="WSS Partner">' . $server_info['name'];
} else {
    $tpl->output['server_name'] = $server_info['name'];
}
$tpl->output['title_name'] = $server_info['name'];
$tpl->output['server_version'] = $server_info['version'];
$tpl->output['server_typ'] = $server_info['type'];
$tpl->output['server_limit'] = $server_info['limit'];
$tpl->output['server_description'] = $server_info['desc'];
$tpl->output['server_added'] = datum($server_info['created']);
$tpl->output['info_added'] = $text['server_added_info'];
if ((int)$server_info['player_checks'] <> 0) {
    $tpl->output['server_online_players'] = $server_info['online_players'];
} else {
    $tpl->output['server_online_players'] = '0';
}
$tpl->output['server_online'] = @round($server_info['online'], 2) . '%';
switch ($server_info['xp']) {
    case 1:
        $tpl->output['server_xp'] = 'Very low';
        break;
    case 2:
        $tpl->output['server_xp'] = 'Low';
        break;
    case 3:
        $tpl->output['server_xp'] = 'Normal';
        break;
    case 4:
        $tpl->output['server_xp'] = 'High';
        break;
    case 5:
        $tpl->output['server_xp'] = 'Very high';
        break;
}
$tpl->output['server_emu'] = $server_info['emu'];
if ($server_info['place'] == 1) {
    $tpl->output['server_place'] = $text['reg_czech'];
} else {
    $tpl->output['server_place'] = $text['reg_abroad'];
}
$tpl->output['server_web'] = '<a href="' . $server_info['web'] . '" target="_blank">' . $server_info['web'] . '</a>';
$tpl->output['server_web_reg'] = '<a href="' . $server_info['web_reg'] . '" target="_blank">' . $server_info['web_reg'] . '</a>';
$tpl->output['server_port'] = $server_info['port'];
$tpl->output['server_owner'] = print_nick($server_info['owner']);
if ($server_info['latency'] == 0) $Latency = '?'; else $Latency = round($server_info['latency']) . 'ms';
$tpl->output['server_latency'] = $Latency;

if (empty($_SESSION['id'])) {
    $vote = false;
} else {
    $c = $db->query_result('SELECT COUNT(*) as `pocet` FROM ratings WHERE (`server_id`= ' . (int)$server_info['id'] . ' AND `user_id`=' . (int)$_SESSION['id'] . ')');
    if ($c == 1) {
        $vote = false;
    } else {
        $vote = true;
    }
}

if ($server_info['ratings'] == 0) {
    $rating = 0;
} else {
    $rating = round($server_info['rating_sum'] / $server_info['ratings'], 2);
}
$rating_width = 30 * $rating;

if ($vote) {
    $tpl->output['rating_stars'] = "<li class='current-rating' title='" . $rating . "/5' style='width:" . $rating_width . "px;'></li>";
    $tpl->output['rating_stars'] .= "<li><a href='action.php?do=rate&amp;server=$id&amp;rating=1' title='1 hvězda z 5' class='jedna-hvezda'></a></li>";
    $tpl->output['rating_stars'] .= "<li><a href='action.php?do=rate&amp;server=$id&amp;rating=2' title='2 hvězdy z 5' class='dve-hvezdy'></a></li>";
    $tpl->output['rating_stars'] .= "<li><a href='action.php?do=rate&amp;server=$id&amp;rating=3' title='3 hvězdy z 5' class='tri-hvezdy'></a></li>";
    $tpl->output['rating_stars'] .= "<li><a href='action.php?do=rate&amp;server=$id&amp;rating=4' title='4 hvězdy z 5' class='ctyri-hvezdy'></a></li>";
    $tpl->output['rating_stars'] .= "<li><a href='action.php?do=rate&amp;server=$id&amp;rating=5' title='5 z 5 hvězd' class='pet-hvezd'></a></li>";
} else {
    $tpl->output['rating_stars'] = "<li class='current-rating' title='$rating/5 " . $text['vote_only'] . "' style='width:" . $rating_width . "px;'></li>
<li><div  title='1 hvězda z 5' class='jedna-hvezda'></div></li>
<li><div  title='2 hvězdy z 5' class='dve-hvezdy'></div></li>
<li><div  title='3 hvězdy z 5' class='tri-hvezdy'></div></li>
<li><div  title='4 hvězdy z 5' class='ctyri-hvezdy'></div></li>
<li><div  title='5 z 5 hvězd' class='pet-hvezd'></div></li>";
}

if (($user['admin'] == 1) OR ($server_info['owner'] == $_SESSION['id'])) {
    $tpl->output['admin'] = '<tr classs="table"><td colspan="2" align="center">' . ulink('edit-server/' . $id, 'Edit') . ' | <a href="#" onclick="potvrd(\'action.php?do=delserver&amp;server=' . $id . '\', \'Opravdu smazat?\'); return false;">Delete</a>';
} else {
    $tpl->output['admin'] = '';
}
if ($user['admin'] == 1) {
    if ($server_info['wss_partner'] == 1) {
        $tpl->output['admin'] .= ' | <a href="action.php?do=delwss&amp;id=' . $id . '">Odebrat WSS</a></td></tr>';
    } else {
        $tpl->output['admin'] .= ' | <a href="action.php?do=addwss&amp;id=' . $id . '">Přidat WSS</a></td></tr>';
    }
} else {
    $tpl->output['admin'] .= '</td></tr>';
}

//comments
$count_comment = $db->query_result('select count(*) as pocet from `comments` where server=' . $id);
$tpl->output['comments'] = '<a href="' . $config['base_url'] . 'index.php?page=comments&amp;server=' . $id . '">Komentáře(' . $count_comment . ')';
?>

