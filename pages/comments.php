<?php
$server = (int) $_GET['server'];
$server_name = $db->query_result('select `name` from `servers` where `id`=' . $server);
$tpl->output['server_name'] = $server_name;
$tpl->output['success_blok'] = 'none';
if (isset($_SESSION['id'])) {
    $tpl->output['add_comment_blok'] = 'block';
    $tpl->output['deny_adding_blok'] = 'display: none';
} else {
    $tpl->output['add_comment_blok'] = 'none';
    $tpl->output['deny_adding_blok'] = '';
    $tpl->output['deny_adding_msg'] = $text['deny_adding_comments'];
}
if ((isset($posted['text'])) and (isset($_SESSION['id']))) {
    $db->insert('comments', array('server' => $server, 'author' => $_SESSION['id'], 'time' => time('now'), 'text' => $posted['text']));
    $tpl->output['success_blok'] = 'block';
    $tpl->output['added_comment'] = $text['added_comment'];
}

$tpl->output['comment_h3'] = $text['comment_h3'];
$tpl->output['add_comment'] = $text['add_comment'];
ob_start();
$num = $db->query_result('select count(*) as pocet from `comments` where `server`=' . $server);
if ($num == 0) {
    echo '<tr><td align="center">' . $text['no_comments'] . '</td></tr>';
} else {
    $p = new CleverPager($db, 'select * from `comments` where `server`=' . $server . ' order by `time` desc', 'str');
    $p->PageSize = 10;
    $p->CountCommand = 'select count(*) as Count from `comments` where `server`=' . $server;
    $p->DataBind();

    while ($row = $p->GetOne()) {
        $row = array_map("htmlspecialchars", $row);
        echo '<tr class="table"><td align="center" style="width:100px">' . print_nick($row['author']) . '<br />' . datum($row['time']) . '</td>';
        echo '<td align="justify" style="width:290px">' . nl2br($db->strip($row['text'])) . '</td>';
        if ($user['admin'] == 1) {
            echo '<td style="text-align:right; width:10px;"><a href="action.php?do=delcom&amp;id=' . $row['id'] . '" title="Smazat" class="del">X</a></td>';
        }
        echo '</tr>';
    }
    echo '<tr><td align="center" colspan="3" class="table">';
    echo $p->DrawPager();
    echo '</td></tr>';
}
$tpl->output['comments_data'] = ob_get_contents();
ob_clean();
$tpl->output['url_to_post'] = 'index.php?page=comments&amp;server=' . $server;
