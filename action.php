<?php
include './includes/config.php';
if (isset($_COOKIE['lang'])) {
    $lang = $_COOKIE['lang'];
} else {
    $lang = 'cs';
}
require('./lang/' . $lang . '.php');
$db = new tMySql;
$db->dbServer = $db_config['host'];
$db->dbUser = $db_config['user'];
$db->dbPassword = $db_config['pass'];
$db->dbName = $db_config['name'];
$db->connect();
$db->query(0, "SET NAMES UTF8");
$geted = $db->escape($_GET);
$posted = $db->escape($_POST);
session_start();
if (isset($_SESSION['id'])) {
    $user = $db->strip($db->query_fetch_assoc('SELECT * from `users` WHERE id =' . (int)$_SESSION['id']));
}
if ($_GET['do'] == 'logout') {
    session_unregister('id');
    header("Location: $_SERVER[HTTP_REFERER]");
}

//check pass
if ($_GET['do'] == 'check') {
    if ($_GET['p1'] == $_GET['p2']) {
        $e = '<div id="img_ok" title="' . $text['pass_ok'] . '"></div>';
    } else {
        $e = '<div id="img_error" title="' . $text['pass_error'] . '"></div>';
    }
    if (empty($_GET['p1'])) {
        $e = '<div id="img_error" title="' . $text['no_pass'] . '"></div>';
    }
    echo $e;
}

// check login
if ($_GET['do'] == 'checkl') {
    $pocet = $db->query_result('SELECT COUNT(*) as pocet FROM `users` WHERE name="' . $geted['login'] . '"');
    if ($pocet == 1) {
        $e = '<div id="img_error" title="' . $text['login_error'] . '"></div>';
    } else {
        $e = '<div id="img_ok" title="' . $text['login_ok'] . '"></div>';
    }
    if (empty($geted['login'])) {
        $e = '<div id="img_error" title="' . $text['no_login'] . '"></div>';
    }
    echo $e;
}

if ($_GET['do'] == 'checke') {
    if (is_email($_GET['email'])) {
        $e = '<div id="img_ok" title="' . $text['email_ok'] . '"></div>';
    } else {
        $e = '<div id="img_error" title="' . $text['email_wrong'] . '"></div>';
    }
    $poce = $db->query_result('SELECT COUNT(*) as pocet FROM `users` WHERE email="' . $geted['email'] . '"');
    if ($poce == 1) {
        $e = '<div id="img_error" title="' . $text['email_used'] . '"></div>';
    }
    echo $e;
}
if ($_GET['do'] == 'confirm') {
    $id = (int)$_GET['id'];
    $db->update('users', array('active' => 1), '`id`*' . $config['nasobitel'] . '=' . $id);
    $name = $db->query_result('SELECT `name` FROM `users` WHERE id = ' . ($id / $config['nasobitel']));
    echo '<link rel="stylesheet" href="./templates/style/screen2.css" type="text/css"  media="screen, projection">';
    echo '<meta http-equiv="content-type" content="text/html; charset=utf-8">';
    if (empty($name)) {
        echo '<span class="error">' . $text['regist_finishing_error'] . '</span>';
    } else {
        echo '<span class="success">' . $name . $text['registr_finished'] . '</span>';
    }
}


//change lang
if ($_GET['do'] == 'language') {
    if (($_GET['lang'] == 'en') OR ($_GET['lang'] == 'cs')) {
        setcookie('lang', $_GET['lang']);
        header("Location: $_SERVER[HTTP_REFERER]");
    } else {
        die('Language not found');
    }
}

if ($_GET['do'] == 'rate') {
    $server_id = (int)$_GET['server'];
    $rating = (int)$_GET['rating'];
    if (empty($_SESSION['id'])) {
        die('Hacking attempt');
    }
    $c = $db->query_result('SELECT COUNT(*) as pocet FROM `ratings` WHERE (`server_id`=' . $server_id . ' AND `user_id`=' . (int)$_SESSION['id'] . ')');
    if (($c == 1) OR ($rating < 1) OR ($rating > 5)) {
        die('Hacking attempt');
    }
    $db->insert('ratings', array('server_id' => $server_id, 'user_id' => $_SESSION['id'], 'rating' => $rating));
    $db->update('servers', array('rating_sum' => 'rating_sum+' . $rating, 'ratings' => 'ratings+1'), 'id=' . $server_id);
    header("Location: $_SERVER[HTTP_REFERER]");
}
if ($_GET['do'] == 'delserver') {
    $id = (int)$_GET['server'];
    if (empty($_SESSION['id'])) {
        die('Hacking attempt2');
    }
    $owner = $db->query_result('SELECT `owner` FROM `servers` WHERE `id`=' . $id);
    if (($owner == $_SESSION['id']) OR ($user['admin'] == 1)) {
        delserver($id);
        echo '<script type="text/javascript"> alert("Smazano"); location.href="' . $config['base_url'] . '";</script>';
    } else {
        die('hacking attempt1');
    }

}

if ($_GET['do'] == 'delcom') {

    if ($user['admin'] == 1) {
        $db->query(0, 'delete from `comments` where `id`=' . (int)$_GET['id']);
        header("Location: $_SERVER[HTTP_REFERER]");
    } else {
        die('hacking atempt');
    }
}

if ($user['admin'] == 1) {
    if ($_GET['do'] == 'delwss') {
        $id = (int)$_GET['id'];
        $db->update('servers', array('wss_partner' => 0), ' `id`=' . $id);
        header("Location: $_SERVER[HTTP_REFERER]");
    }
    if ($_GET['do'] == 'addwss') {
        $id = (int)$_GET['id'];
        $db->update('servers', array('wss_partner' => 1), ' `id`=' . $id);
        header("Location: $_SERVER[HTTP_REFERER]");
    }
    if ($_GET['do'] == 'ban') {
        $id = (int)$_GET['id'];
        $db->update('users', array('ban' => 1), '`id`=' . $id);
        header("Location: $_SERVER[HTTP_REFERER]");
    }
    if ($_GET['do'] == 'unban') {
        $id = (int)$_GET['id'];
        $db->update('users', array('ban' => 0), '`id`=' . $id);
        header("Location: $_SERVER[HTTP_REFERER]");
    }
}

?>
